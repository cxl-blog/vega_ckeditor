CKEDITOR.plugins.add('xiumi', {
    lang: 'zh-cn',
    icons: 'xiumi',
    init: function(editor) {
        CKEDITOR.dialog.add('xiumi', this.path + 'dialogs/xiumi.js');
        editor.addCommand('xiumi', new CKEDITOR.dialogCommand('xiumi'));
        editor.ui.addButton('xiumi', {
            label: editor.lang.xiumi.toolbar,
            command: 'xiumi'
        });
    }
});