CKEDITOR.dialog.add('xiumi', function (editor) {
    var lang = editor.lang.xiumi;
    var xiumi_url = "http://xiumi.us";
    var saveUrl = editor.config.xiumiImageUploadUrl;
    function saveImg(event) {
        if (event.origin !== undefined && event.origin == xiumi_url) {
            console.log('秀米提交事件:', event);
            $.post(saveUrl, {data: event.data}, function (res) {
                if (res.data) {
                    // todo
                    $('#cke_' + editor.name).find('iframe').contents().find('body [powered-by="xiumi.us"]').parent('section').remove();
                    editor.insertHtml(`<p>${res.data}</p>`);
                    window.parent.CKEDITOR.dialog.getCurrent().hide();
                }
            })
        }
    }
    window.removeEventListener('message', saveImg, false);

    var initJs = function () {
        document.addEventListener("mousewheel", function (event) {
            // event.preventDefault();
            // event.stopPropagation();
        });

        if (saveUrl !== undefined || saveUrl !== '') {
            window.addEventListener('message', saveImg, false);
        }
    };

    var dialogDefinition = {
        title: editor.lang.xiumi.title + "(编辑完成请点击秀米页面中的【复制到微信公众号】按钮)",
        minWidth: 1140,
        minHeight: 750,
        resizable: CKEDITOR.DIALOG_RESIZE_BOTH,
        buttons: [CKEDITOR.dialog.cancelButton, CKEDITOR.dialog.okButton],
        contents: [{
            id: 'xiumi',
            label: editor.lang.xiumi.title,
            title: editor.lang.xiumi.title,
            expand: true,
            elements: [{
                id: "body",
                type: "html",
                html: '<div class="js-xiumi-body"></div>'
            }]
        }],

        onLoad: function () {
            $('.' + editor.id + ' .js-xiumi-body').css({'vertical-align': 'top'});
            $('.' + editor.id + ' .js-xiumi-body').load(CKEDITOR.getUrl('plugins/xiumi/html/index_' + editor.config.language + '.html'), initJs());
        },

        onOk: function () {

        }
    };

    return dialogDefinition;
});
