vega-CKEditor
==========

CKEditor-based implementation  and  add some plugins, For example kityformula etc. 

## Installation

```shell
$ npm install vega-ckeditor
```

### 集成了秀米编辑功能(并且实现本土本地保存)
```
前台配置：
 var ckeditor = CKEDITOR.replace('body', {
        toolbar: 'Admin',
        allowedContent: true,
        resize_enabled: true,
        disableObjectResizing: true,
        xiumiImageUploadUrl: xiumiSaveUrl,
        height: 300
    });
    
    xiumiImageUploadUrl:为后台接收编辑内容并实现图片本地保存的功能的api
    post提交 参数data
    
    php后台示例：
    public function xiumiAction()
        {
            $data = $this->request->post('data');
    
            if (empty($data)) {
                return json();
            }
    
            // 匹配秀米
            $preg = '/http:\/\/statics.xiumi[\s\S]*?\b(gif|png|jpeg|gif|bmp|icon)\b/i';
            preg_match_all($preg, $data, $res);
    
            if (empty($res)) {
                return json('ok');
            }
    
            foreach ($res[0] as $re) {
                if (empty($re)) {
                    continue;
                }
    
                /**
                 * uri 未保存到本地图片的地址
                 */
                $uri = $this->getFileModel()->saveXiumiFile(trim(str_replace("\"", "", $re)));
                $data = str_replace($re, $uri, $data);
            }
    
            return json(['data' => $data]);
        }
```